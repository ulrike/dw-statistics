#!/usr/bin/env python3

# Copyright 2021 Ulrike Uhlig
#
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See http://www.wtfpl.net/
# for more details.

"""
This script regenerates the graph shown on the `Debian Women Statistics
page <https://wiki.debian.org/DebianWomen/Projects/Statistics> based on
an idea by Elena Grandi (valhalla) and rewritten from scratch by Ulrike
Uhlig (ulrike).

It parses the HTML version of the page looking for the title IDs
"Women_that_have_at_some_point_maintained_packages", "Women with DD account",
"Women with DM keys" and then taking data points from list items.  Lines that
include the words "locked" or "emeritus" are counted as a negative value.

It requires python-matplotlib, urrlib3 and BeautifulSoup.

To use it, just run the script and then upload the resulting png file.
"""

from bs4 import BeautifulSoup
import datetime
import urllib3

import matplotlib.pyplot as plt

URL = 'https://wiki.debian.org/DebianWomen/Projects/Statistics'

# Fetch the webpage
http = urllib3.PoolManager()
r = http.request("GET", URL)
soup = BeautifulSoup(r.data, 'html.parser')

def create_people_list(name):
    '''
    Generates a list of people from the DW Statistics Wiki URL
    including year, full names, and emeritus information.
    '''
    people = list()
    t = soup.find(id=name)
    ul = t.findNext('ul')
    persons = ul.findChildren("li" , recursive=False)
    for person in persons:
        person_data = person.text.split(" ", 1) # split into date and name
        date = person_data[0] # date
        details = person_data[1].rstrip() # remove whitespace from details
        try:
            datetime.datetime.strptime(date, '%Y-%m-%d')
        except ValueError:
            continue
        people.append((date, details))
        people.sort()

    return people

def create_plot_data(name):
    '''
    Transforms the list of people into a flat list of tuples and then into a list
    that can be fed to matplot. Takes into account the years when people left the project
    - using the word "emeritus", which, in female should be emerita…
    '''
    persons = create_people_list(name)
    flat_data = list()
    plot_data = list()
    person_count = 0

    for person in persons:
        year_joined_tmp = person[0].split("-", 1)
        year_j = int(year_joined_tmp[0])
        v = 1
        flat_data.append((year_j, v))

        if 'emeritus' in person[1] or 'locked' in person[1]:
            person_data = person[1].split("-- ", 1) # we need to extract the year of retirement
            year_left = person_data[1].split(" ")
            year_l = int(year_left[0])
            v = -1
            flat_data.append((year_l, v))

    flat_data_sorted = sorted(flat_data, key=lambda x: x[0])

    for count in flat_data_sorted:
        year = count[0]
        person_count = person_count + count[1]
        plot_data.append((year, person_count))

    unzipped_object = zip(*plot_data)
    unzipped_plot_data = list(unzipped_object)
    return unzipped_plot_data

# Create plottable lists using HTML <h2> title id attributes.
people_dd = create_plot_data('Women_with_DD_account')
people_dm = create_plot_data('Women_with_DM_keys')
people_uploading = create_plot_data('Women_that_have_at_some_point_maintained_packages')

# Graph params
plt.xlabel('Year')
plt.ylabel('Number of people')
plt.title('Female participation in Debian')
plt.xlim(1995, 2022)
plt.ylim(0, 95)
plt.grid(True)

# Generate the graph
plt.plot(people_uploading[0], people_uploading[1], color='blue', label='First uploads')
plt.plot(people_dd[0], people_dd[1], color='red', label='DD account')
plt.plot(people_dm[0], people_dm[1], color='cyan', label='DM key')
plt.fill_between(people_uploading[0], people_uploading[1], 0, facecolor='blue', alpha=0.1)
plt.fill_between(people_dd[0], people_dd[1], 0, facecolor='red', alpha=0.1)
plt.fill_between(people_dm[0], people_dm[1], 0, facecolor='cyan', alpha=0.5)
# show last value
for var in (people_dd[1], people_uploading[1]):
    plt.annotate('%0i' % var[-1], xy=(1, var[-1]), xytext=(8, 0),
        xycoords=('axes fraction', 'data'), textcoords='offset points')

plt.legend()
plt.show()
