#!/usr/bin/env python

# Copyright 2013 Elena Grandi
# 
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See http://www.wtfpl.net/ 
# for more details.

"""
This script regenerates the graph shown on the `Debian Women Statistics 
page <https://wiki.debian.org/DebianWomen/Projects/Statistics>`_.

It parses the raw text version of the page looking for the titles 
"Women that have at some point maintained packages", 
"Women with DD account", "Women with DM keys" and 
"Latest uploads from DD accounts" (the latter to stop parsing) 
and then taking data points from lines starting with " *" 
followed by a date.
Lines that include the words "locked" or "resigned" are counted 
as a negative value.

It requires python-matplotlib >= 1.3, for the plt.xkcd() command.

To use it, just run the script and then upload the resulting png file.
"""

import datetime
import urllib2

import matplotlib.pyplot as plt
import matplotlib.font_manager

URL = "https://wiki.debian.org/DebianWomen/Projects/Statistics?action=raw"

FNAME = "dwstats.png"

class DWParser:

    def parse(self,url):
        self._reset_values()
        self.status = 'START'
        up = urllib2.urlopen(url)
        for line in up.readlines():
            if 'Women that have at some point maintained packages' in line:
                self.status = 'UPLOADS'
                continue
            elif 'Women with DD account' in line:
                self.status = 'DD'
                continue
            elif 'Women with DM keys' in line:
                self.status = 'DM'
                continue
            elif 'Latest uploads from DD accounts' in line:
                self.status = 'DONE'
                break
            if line.startswith(' *'):
                try:
                    date = datetime.datetime.strptime( line.split()[1],
                            '%Y-%m-%d').date()
                except ValueError:
                    continue
                if 'locked' in line or 'resigned' in line:
                    v = -1
                else:
                    v = 1
                if self.status == 'UPLOADS':
                    self.uploads.append((date,v))
                elif self.status == 'DD':
                    self.dd.append((date,v))
                elif self.status == 'DM':
                    self.dm.append((date,v))
        self.uploads.sort()
        self.dd.sort()
        self.dm.sort()

    def _reset_values(self):
        self.uploads = []
        self.dd = []
        self.dm = []


class GraphGenerator:
    
    def __init__(self):
        plt.xkcd()
        self.fig = plt.figure(figsize=(8.38,6.24))
        plt.xlim(datetime.date(1996,01,01), datetime.date.today())
        self.font = matplotlib.font_manager.FontProperties(
                family='Sans',
                weight='normal')

    def add_line(self,dates,label):
        x = [d[0] for d in dates] + [datetime.date.today()]
        y_st = [d[1] for d in dates] + [0]
        y = [sum(y_st[:i+1]) for i in xrange(len(y_st))]
        plt.plot(x, y, label = label)

    def save(self,fname):
        plt.legend(loc='upper left', prop=self.font)
        ax2 = plt.twinx()
        ax2.set_ylim(self.fig.axes[0].get_ylim())
        for ax in self.fig.axes:
            for label in ax.get_xticklabels():
                label.set_fontproperties(self.font)
            for label in ax.get_yticklabels():
                label.set_fontproperties(self.font)
        plt.savefig(fname)


def main():
    p = DWParser()
    p.parse(URL)
    g = GraphGenerator()
    g.add_line(p.uploads,"First uploads")
    g.add_line(p.dd,"DD Account")
    g.add_line(p.dm,"DM key")
    g.save(FNAME)

if __name__ == '__main__': main()
